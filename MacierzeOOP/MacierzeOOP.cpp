#include "Program.h"
#include <iostream>

int main()
{
	Program<int> p( 50, 0, 49 );

	p.czytaj_dane();

	p.przetworz_dane();

	p.wyswietl_dane();

	return 0;
}
