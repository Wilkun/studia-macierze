#ifndef __AHE_PROGRAM_H
#define __AHE_PROGRAM_H

#include "Matrix.h"

template <typename T>
class Program
{
private:
	Matrix<int> A;
	Matrix<int> B;
	unsigned rozmiar;
	unsigned from;
	unsigned to;

	std::vector<int> sum1;
	std::vector<int> sum2;
	unsigned sum3;
	unsigned sum4;
	unsigned sum5;
	unsigned sum6;

public:
	Program(unsigned _rozmiar, unsigned _from, unsigned _to);
	virtual ~Program();

	void czytaj_dane();
	void przetworz_dane();
	void wyswietl_dane();
};

#include "Program.cpp"

#endif

