#ifndef __AHE_MATRIX_H
#define __AHE_MATRIX_H

#include <vector>

template <typename T>
class Matrix {
private:
	std::vector<std::vector<T> > mat;
	unsigned rows;
	unsigned cols;

public:
	Matrix();
	Matrix(unsigned _rows, unsigned _cols, const T& _initial);
	Matrix(const Matrix<T>& rhs);
	virtual ~Matrix();

	// Operator overloading, for "standard" mathematical matrix operations                                                                                                                                                          
	Matrix<T>& operator=(const Matrix<T>& rhs);

	// Matrix mathematical operations                                                                                                                                                                                               
	Matrix<T> operator+(const Matrix<T>& rhs);
	Matrix<T>& operator+=(const Matrix<T>& rhs);
	Matrix<T> operator-(const Matrix<T>& rhs);
	Matrix<T>& operator-=(const Matrix<T>& rhs);
	Matrix<T> operator*(const Matrix<T>& rhs);
	Matrix<T>& operator*=(const Matrix<T>& rhs);
	Matrix<T> transpose();

	// Matrix/scalar operations                                                                                                                                                                                                     
	Matrix<T> operator+(const T& rhs);
	Matrix<T> operator-(const T& rhs);
	Matrix<T> operator*(const T& rhs);
	Matrix<T> operator/(const T& rhs);

	// Matrix/vector operations                                                                                                                                                                                                     
	std::vector<T> operator*(const std::vector<T>& rhs);
	std::vector<T> diag_vec();

	std::vector<T> sum_positive_col() const;
	std::vector<T> sum_negative_row() const;
	T sum_positive_main_diag_vec() const;
	T sum_sub_diag_vec() const;
	T sum_above_main_diag_vec() const;
	T sum_below_main_diag_vec() const;

	// Access the individual elements                                                                                                                                                                                               
	T& operator()(const unsigned& row, const unsigned& col);
	const T& operator()(const unsigned& row, const unsigned& col) const;

	// Access the row and column sizes                                                                                                                                                                                              
	unsigned get_rows() const;
	unsigned get_cols() const;

	// display the matrix
	void display() const;

	T randomizer(T _from, T _to) const;
	void fill_with_random(T _from, T _to);
};
#include "matrix.cpp"

#endif