#ifndef __AHE_PROGRAM_CPP
#define __AHE_PROGRAM_CPP

#include "Program.h"
#include <algorithm>
#include <iterator>

// Parameter Constructor                                                                                                                                                      
template<typename T>
Program<T>::Program(unsigned _rozmiar, unsigned _from, unsigned _to) {
	rozmiar = _rozmiar;
	from = _from;
	to = _to;
}

// (Virtual) Destructor                                                                                                                                                       
template<typename T>
Program<T>::~Program() {}

template<typename T>
void Program<T>::czytaj_dane()
{
	A = Matrix<int>(rozmiar, rozmiar, 1.0);

	A.fill_with_random(from, to);

	B = Matrix<int>(rozmiar, rozmiar, 0);
}

template<typename T>
void Program<T>::przetworz_dane()
{
	B = A.transpose() + B;

	B.display();

	sum1 = B.sum_positive_col();

	sum2 = B.sum_negative_row();

	sum3 = B.sum_positive_main_diag_vec();

	sum4 = B.sum_sub_diag_vec();

	sum5 = B.sum_above_main_diag_vec();

	sum6 = B.sum_below_main_diag_vec();

}

template<typename T>
void Program<T>::wyswietl_dane()
{
	std::cout << "1) suma liczb(> 0) - w kazdy kolumnie" << std::endl;

	std::copy(sum1.begin(), sum1.end(),
		std::ostream_iterator<int>(std::cout, " "));

	std::cout << "\n" << std::endl;


	std::cout << "2) suma liczb(< 0) - w kazdy wiersze" << std::endl;

	std::copy(sum2.begin(), sum2.end(),
		std::ostream_iterator<int>(std::cout, " "));

	std::cout << "\n" << std::endl;

	std::cout << "3) suma liczb(> 0) znajdujacych sie na glownej przekatnej: " << sum3 << "\n" << std::endl;

	std::cout << "4) suma liczb znajdujacych sie na nie glownej przekatnej: " << sum4 << "\n" << std::endl;

	std::cout << "5) suma liczb znajdujacych sie ponizej glownej przekatnej: " << sum5 << "\n" << std::endl;

	std::cout << "6) suma liczb znajdujacych sie powyzej glownej przekatnej: " << sum6 << "\n" << std::endl;
}

#endif