#ifndef __AHE_MATRIX_CPP
#define __AHE_MATRIX_CPP

#include "Matrix.h"
#include <random>
#include <iostream>

// No-Parameter Constructor                                                                                                                                                      
template<typename T>
Matrix<T>::Matrix() {
	unsigned _rows = 2;
	unsigned _cols = 2;
	const T _initial = 0;
	mat.resize(_rows);
	for (unsigned i = 0; i < mat.size(); i++) {
		mat[i].resize(_cols, _initial);
	}
	rows = _rows;
	cols = _cols;
}

// Parameter Constructor                                                                                                                                                      
template<typename T>
Matrix<T>::Matrix(unsigned _rows, unsigned _cols, const T& _initial) {
	mat.resize(_rows);
	for (unsigned i = 0; i < mat.size(); i++) {
		mat[i].resize(_cols, _initial);
	}
	rows = _rows;
	cols = _cols;
}

// Copy Constructor                                                                                                                                                           
template<typename T>
Matrix<T>::Matrix(const Matrix<T>& rhs) {
	mat = rhs.mat;
	rows = rhs.get_rows();
	cols = rhs.get_cols();
}

// (Virtual) Destructor                                                                                                                                                       
template<typename T>
Matrix<T>::~Matrix() {}

// Assignment Operator                                                                                                                                                        
template<typename T>
Matrix<T>& Matrix<T>::operator=(const Matrix<T>& rhs) {
	if (&rhs == this)
		return *this;

	unsigned new_rows = rhs.get_rows();
	unsigned new_cols = rhs.get_cols();

	mat.resize(new_rows);
	for (unsigned i = 0; i < mat.size(); i++) {
		mat[i].resize(new_cols);
	}

	for (unsigned i = 0; i < new_rows; i++) {
		for (unsigned j = 0; j < new_cols; j++) {
			mat[i][j] = rhs(i, j);
		}
	}
	rows = new_rows;
	cols = new_cols;

	return *this;
}

// Addition of two matrices                                                                                                                                                   
template<typename T>
Matrix<T> Matrix<T>::operator+(const Matrix<T>& rhs) {
	Matrix result(rows, cols, 0.0);

	for (unsigned i = 0; i < rows; i++) {
		for (unsigned j = 0; j < cols; j++) {
			result(i, j) = this->mat[i][j] + rhs(i, j);
		}
	}

	return result;
}

// Cumulative addition of this matrix and another                                                                                                                             
template<typename T>
Matrix<T>& Matrix<T>::operator+=(const Matrix<T>& rhs) {
	unsigned rows = rhs.get_rows();
	unsigned cols = rhs.get_cols();

	for (unsigned i = 0; i < rows; i++) {
		for (unsigned j = 0; j < cols; j++) {
			this->mat[i][j] += rhs(i, j);
		}
	}

	return *this;
}

// Subtraction of this matrix and another                                                                                                                                     
template<typename T>
Matrix<T> Matrix<T>::operator-(const Matrix<T>& rhs) {
	unsigned rows = rhs.get_rows();
	unsigned cols = rhs.get_cols();
	Matrix result(rows, cols, 0.0);

	for (unsigned i = 0; i < rows; i++) {
		for (unsigned j = 0; j < cols; j++) {
			result(i, j) = this->mat[i][j] - rhs(i, j);
		}
	}

	return result;
}

// Cumulative subtraction of this matrix and another                                                                                                                          
template<typename T>
Matrix<T>& Matrix<T>::operator-=(const Matrix<T>& rhs) {
	unsigned rows = rhs.get_rows();
	unsigned cols = rhs.get_cols();

	for (unsigned i = 0; i < rows; i++) {
		for (unsigned j = 0; j < cols; j++) {
			this->mat[i][j] -= rhs(i, j);
		}
	}

	return *this;
}

// Left multiplication of this matrix and another                                                                                                                              
template<typename T>
Matrix<T> Matrix<T>::operator*(const Matrix<T>& rhs) {
	unsigned rows = rhs.get_rows();
	unsigned cols = rhs.get_cols();
	Matrix result(rows, cols, 0.0);

	for (unsigned i = 0; i < rows; i++) {
		for (unsigned j = 0; j < cols; j++) {
			for (unsigned k = 0; k < rows; k++) {
				result(i, j) += this->mat[i][k] * rhs(k, j);
			}
		}
	}

	return result;
}

// Cumulative left multiplication of this matrix and another                                                                                                                  
template<typename T>
Matrix<T>& Matrix<T>::operator*=(const Matrix<T>& rhs) {
	Matrix result = (*this) * rhs;
	(*this) = result;
	return *this;
}

// Calculate a transpose of this matrix                                                                                                                                       
template<typename T>
Matrix<T> Matrix<T>::transpose() {
	Matrix result(rows, cols, 0.0);

	for (unsigned i = 0; i < rows; i++) {
		for (unsigned j = 0; j < cols; j++) {
			result(i, j) = this->mat[j][i];
		}
	}

	return result;
}

// Matrix/scalar addition                                                                                                                                                     
template<typename T>
Matrix<T> Matrix<T>::operator+(const T& rhs) {
	Matrix result(rows, cols, 0.0);

	for (unsigned i = 0; i < rows; i++) {
		for (unsigned j = 0; j < cols; j++) {
			result(i, j) = this->mat[i][j] + rhs;
		}
	}

	return result;
}

// Matrix/scalar subtraction                                                                                                                                                  
template<typename T>
Matrix<T> Matrix<T>::operator-(const T& rhs) {
	Matrix result(rows, cols, 0.0);

	for (unsigned i = 0; i < rows; i++) {
		for (unsigned j = 0; j < cols; j++) {
			result(i, j) = this->mat[i][j] - rhs;
		}
	}

	return result;
}

// Matrix/scalar multiplication                                                                                                                                               
template<typename T>
Matrix<T> Matrix<T>::operator*(const T& rhs) {
	Matrix result(rows, cols, 0.0);

	for (unsigned i = 0; i < rows; i++) {
		for (unsigned j = 0; j < cols; j++) {
			result(i, j) = this->mat[i][j] * rhs;
		}
	}

	return result;
}

// Matrix/scalar division                                                                                                                                                     
template<typename T>
Matrix<T> Matrix<T>::operator/(const T& rhs) {
	Matrix result(rows, cols, 0.0);

	for (unsigned i = 0; i < rows; i++) {
		for (unsigned j = 0; j < cols; j++) {
			result(i, j) = this->mat[i][j] / rhs;
		}
	}

	return result;
}

// Multiply a matrix with a vector                                                                                                                                            
template<typename T>
std::vector<T> Matrix<T>::operator*(const std::vector<T>& rhs) {
	std::vector<T> result(rhs.size(), 0.0);

	for (unsigned i = 0; i < rows; i++) {
		for (unsigned j = 0; j < cols; j++) {
			result[i] = this->mat[i][j] * rhs[j];
		}
	}

	return result;
}

// Obtain a vector of the diagonal elements                                                                                                                                   
template<typename T>
std::vector<T> Matrix<T>::diag_vec() {
	std::vector<T> result(rows, 0.0);

	for (unsigned i = 0; i < rows; i++) {
		result[i] = this->mat[i][i];
	}

	return result;
}

template<typename T>
std::vector<T> Matrix<T>::sum_positive_col() const
{
	std::vector<T> result(cols, 0.0);

	for (T i = 0; i < this->cols; i++) {

		T sum = 0;

		for (T j = 0; j < this->rows; j++) {

			if (this->mat[j][i] > 0) {

				sum += this->mat[j][i];

			}
		}

		result[i] = sum;
	}

	return result;
}

template<typename T>
std::vector<T> Matrix<T>::sum_negative_row() const
{
	std::vector<T> result(rows, 0.0);

	for (T i = 0; i < this->rows; i++) {

		T sum = 0;

		for (T j = 0; j < this->cols; j++) {

			if (this->mat[i][j] < 0) {

				sum += this->mat[i][j];

			}
		}

		result[i] = sum;
	}

	return result;
}

template<typename T>
T Matrix<T>::sum_positive_main_diag_vec() const
{
	unsigned sum = 0.0;
	for (unsigned i = 0; i < rows; i++) {
		if (this->mat[i][i] > 0) {
			sum += this->mat[i][i];
		}
	}

	return sum;
}

template<typename T>
T Matrix<T>::sum_sub_diag_vec() const
{
	unsigned sum = 0.0;
	unsigned last_row = rows - 1;

	for (unsigned i = 0; i < rows; i++) {

		if (this->mat[last_row - i][i] > 0) {
			sum += this->mat[last_row - i][i];
		}
	}

	return sum;
}

template<typename T>
T Matrix<T>::sum_above_main_diag_vec() const
{
	unsigned sum = 0.0;
	for (unsigned i = 0; i < this->rows; i++) {
		for (unsigned j = 0; j < this->cols; j++) {
			if ( i < j) {
				sum += this->mat[i][j];
			}
		}
	}
	return sum;
}

template<typename T>
T Matrix<T>::sum_below_main_diag_vec() const
{
	unsigned sum = 0.0;
	for (unsigned i = 0; i < this->rows; i++) {
		for (unsigned j = 0; j < this->cols; j++) {
			if (i > j) {
				sum += this->mat[i][j];
			}
		}
	}
	return sum;
}

// Access the individual elements                                                                                                                                             
template<typename T>
T& Matrix<T>::operator()(const unsigned& row, const unsigned& col) {
	return this->mat[row][col];
}

// Access the individual elements (const)                                                                                                                                     
template<typename T>
const T& Matrix<T>::operator()(const unsigned& row, const unsigned& col) const {
	return this->mat[row][col];
}

// Get the number of rows of the matrix                                                                                                                                       
template<typename T>
unsigned Matrix<T>::get_rows() const {
	return this->rows;
}

// Get the number of columns of the matrix                                                                                                                                    
template<typename T>
unsigned Matrix<T>::get_cols() const {
	return this->cols;
}

template<typename T>
void Matrix<T>::display() const
{
	for (T i = 0; i < this->rows; i++) {
		for (T j = 0; j < this->cols; j++) {
			std::cout << this->mat[i][j] << ",\t";
		}
		std::cout << std::endl;
	}
}

template<typename T>
T Matrix<T>::randomizer(T _from, T _to) const
{
	static std::random_device rd;
	static std::mt19937 gen(rd());
	static std::uniform_int_distribution<T> dis{ _from, _to };

	return dis(gen);
}

template<typename T>
void Matrix<T>::fill_with_random(T _from, T _to)
{
	for (T i = 0; i < this->rows; i++) {
		for (T j = 0; j < this->cols; j++) {
			this->mat[i][j] = randomizer(_from, _to);
		}
	}
}

#endif